<?php

$host = "localhost";
$user = "root";
$password = "bgehWgsd";
$database = "admins";

$connect = mysqli_connect($host, $user, $password, $database);

if (mysqli_connect_errno()) {
    die('Cannot connect to database field:' . mysqli_connect_error());
}

?>


<html>

<body style='background-color: #737373; display: flex; justify-content:center; flex-direction:column; height: 90%'>
    <?php

    session_start();

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $username = $_POST['username'];
        $password = $_POST['password'];

        $query = 'select * from logins where username = "' . $username . '"';
        $result = mysqli_query($connect, $query);

        $getUserInformations = mysqli_fetch_assoc($result);

        if (!$getUserInformations) {
            echo '<div style="display: flex; justify-content:center; color: white">
            <h3>This Username Is Not In Our Records, Please Sign Up And Try Again !</h3>
            </div>';;
            header('REFRESH:4;URL=loginPage.php');
        } else {
            $getUserNameFromQuery = $getUserInformations['username'];
            $getPasswordFromQuery = $getUserInformations['password'];

            if ($username === $getUserNameFromQuery && $password === $getPasswordFromQuery) {
                $_SESSION['user'] = $username;
                echo  '<div style="display: flex; justify-content:center;">
                    <h3>Welcome <span style="color: white">' . $username . '</span> .. You will be redirected to see your informations</h3>
                    </div>';

                header('REFRESH:4;URL=userInformations.php');
            } else {
                echo '<div style="display: flex; justify-content:center; color: white">
                <h3>Username Or Password Is Incorrect, Please Try Again!</h3>
                </div>';
                header('REFRESH:4;URL=loginPage.php');
            }
        }
    } else {
        echo '<div style="display: flex; justify-content:center; flex-direction:column; height: 90%">
            <h1 style="text-align: center; color: white">Error: You can\'t check this page directly, Please Login First
            <a style="margin-left: 10px; color: #01DCFE" href="loginPage.php">Login Page</a>
            </h1>
            </div>';
    }
    ?>

</body>

</html>

<?php
mysqli_close($connect)
?>